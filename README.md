# Utilisation

## Environnement de développement

### Configuration

Dans le dossier `src`, copier le fichier `.env.example` vers un nouveau fichier `.env` et effectuer les éventuelles modifications correspondant à des besoins spécifiques.

S'assurer que les variables concernant la base de données aient les mêmes valeurs dans les fichiers `src/.env` et `docker/topologies/dev/docker-compose.yml`.

### Lancement de l'environnement

Après avoir cloné le repository, le script `launch-dev-environment.sh` permet de rapidement exécuter l'application dans un environnement de développement. Il requiert uniquement une installation fonctionnelle de Docker.

L'usage est le suivant :

```
./launch-dev-environment.sh [OPTIONS]

Options:
    -i      Procède à l'installation des dépendances (node, php) et installe
            la clé d'application avant de lancer l'application
    -d      Lance l'application en mode détaché
    -s      Effectue les migrations et peuple la base de données avant
            de lancer l'application
    -u      Met à jour les dépendances (node, php) avant de lancer
            l'application
```

> Note : pour la première utilisation, il est important d'installer les dépendances et d'effectuer les migrations :
`./launch-dev-environment.sh -is`.

L'application est ensuite accessible sur `localhost:8008`.

### Redémarrer l'application

Il est possible d'agir directement sur le container nommé pour redémarrer l'application :

```
docker restart brokenfeature
```

Compatible également avec les commandes Docker `start` et `stop`.

### Arrêt complet

Le script `./compose-down.sh` est un raccourci pour `docker-compose down` dans le dossier de la topologie dev. 

```
./compose-down.sh [OPTIONS]

Options :
    -v      Supprime également les volumes attachés à la topologie
    ...     Toutes les autres options de la commande down sont
            également disponibles (voir documentation)
```

### Tests

#### Tests d'intégration

Les tests d'intégration utilisent PhpUnit. On peut les lancer avec la commande suivante :  
```
./run.sh test
```

#### Tests 'End-to-End'

Les test E2E sont créés avec la librairie *Laravel Dusk* et utilisent une instance locale de *ChromeDriver* version 86.

Le lancement des tests s'effectue avec la command suivante :

```
./run.sh dusk
```

### Commandes indépendantes

Pour effectuer rapidement des commandes de maintenance (node, php, ...), le script `run.sh` fournit un raccourci vers les services les plus courants :

```
./run.sh [SERVICE] [COMMANDE]

Les SERVICES disponibles sont les suivants :
    npm         Exécute une commande npm dans le dossier src
    artisan     Exécute une commande artisan dans le dossier src
    composer    Exécute une commande composer dans le dossier src

Pour les COMMANDES disponibles pour chaque service, se référer à la
documentation officielle du service concerné.

Exemple :
    ./run.sh artisan route:list
```

> Note : pour des raisons de dépendances, il n'est pas garanti que toutes les commandes d'un service soient supportées.

## Environnement de production

Voir [ce répertoire](https://gitlab.com/broken-feature/docker-deployment) pour un déploiement en production.
