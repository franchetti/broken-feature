#!/bin/sh

print () {
    echo "[$(date +'%d.%m.%Y %T')] $@"
}

if [ "${SEED}" == true ]
then
    print "Database will be seeded at migration"
    SEED="--seed"
else
    SEED=""
fi

if [ "$(cat .env | grep APP_KEY=base64)" == "" ]
then
    print "App key not found, generating..."
    php artisan key:generate
fi

print "Making storage available from the public directory..."
php artisan storage:link

export $(cat .env | grep DB_HOST)
if [ "$(getent hosts $DB_HOST | awk '{ print $1 }')" != "" ]
then
    while [[ "$(cat /home/laravel/db_log/db.log | grep -F '[services.d] done')" == "" \
        || "$(php artisan migrate:status | grep Exception)" != "" \
        || "$(php artisan migrate:status | grep 'Connection refused')" != "" ]]
    do
        print "Waiting for database availability..." 
        sleep 1
    done 

    if [ "$(php artisan migrate:status | grep 'not found')" != "" ]
    then
        print "No migration found. Migrating..."
        php artisan migrate:fresh $SEED
    else
        print "Database already filled"
    fi
else
    print "Database unreachable, is this container launched as a standalone?"
fi

print "Optimizing cache..."
php artisan config:cache
php artisan route:cache

docker-php-entrypoint php-fpm