#!/bin/bash

COMPOSE_FILE=docker/topologies/dev/docker-compose.yml
export ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $ROOT_DIR

docker-compose -f $COMPOSE_FILE down $@
