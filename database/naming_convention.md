# Naming conventions

Elements describing a specific model (i.e. controllers and tables) use its name in plural or singular form.

| Object               | Case       | Form       | Example          |
| -:                   | :-:        | :-:        | :-               |
| **Models**           | camelCase  | Singular   | `User`           |
| **Controllers**      | camelCase  | Singular   | `UserController` |
| **Tables**           | snake_case | Plural     | `users`          |
| **Table columns**    | snake_case | (Singular) | `title`          |
| **Pivot tables**\*   | snake_case | Singular   | `project_user`   |
| **Primary keys**\*\* | snake_case | (Singular) | `id`             |
| **Foreign keys**     | snake_case | Singular   | `user_id`        |

---

\* Must be in alphabetical order.  
\*\* Should only be named `id`.

---

*Further reference: [Laravel Migrations Documentation](https://laravel.com/docs/6.x/migrations) and [Web Dev etc](https://webdevetc.com/blog/laravel-naming-conventions/#section_naming-database-tables-in-laravel).*