<?php

namespace App\Http\Controllers;

use App\Helpers\AuthHelpers;
use App\Helpers\CommentHelper;
use App\Helpers\FeatureHelper;
use App\Helpers\ProjectHelper;
use App\Helpers\RatingHelper;
use App\Project;
use App\Update;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class UpdateController extends Controller
{
    const COMMENTS_PER_LEVEL = 3;
    const COMMENTS_SUBLEVELS = 1;

    public function show($username, $projectSlug, $updateId)
    {
        // Search related project and user
        $user = User::where('username', $username)->firstOrFail();
        $project = $user->projects()->where('slug', $projectSlug)->firstOrFail();
        $update = $project->updates()->findOrfail($updateId);

        // Calculate update's rating and other stats
        $update->rating = RatingHelper::calculateRating($update);
        $update->features_count = count($update->features);
        $update->comments_count = 0;
        foreach ($update->features as $f) {
            // Current user's vote on feature
            $f->userVote = FeatureHelper::checkUserVote($f->id);

            // Create comment structure
            $f->root_comments = CommentHelper::createCommentStructure($f->id, true);
            $update->comments_count += count($f->comments);
            // Extract top comment
            if(count($f->root_comments) > 0) {
                $f->top_comment = $f->root_comments[0];
                $array = $f->root_comments;
                array_shift($array);
                $f->root_comments = $array; // Remove first entry
            }

            // Check if not all comments are being display for now (+1 is for the best comment that's been extracted)
            $f->nb_more_comments = max(0, count($f->root_comments) + 1 - self::COMMENTS_PER_LEVEL);
        }

        return view('updates/show', [
            'update' => $update,
        ]);
    }

    /**
     * Display the create update view
     */
    public function create($projectId){

        $project = $this->checkUserCanDoThis($projectId);

        return view('updates/create', [
            'projectName' => $project['name'],
            'projectId' => $projectId
        ]);
    }

    /**
     * Validate and store a new update resource
     * @param Request $request The http request
     * @return Redirect to view
     */
    public function store($projectId, Request $request){
        $project = $this->checkUserCanDoThis($projectId);

        $request->validate([
            'version' => 'required|string|max:15',
            'releaseDate' => 'required|regex:'.ProjectHelper::$DATE_REGEX,
            'name' => 'string|max:50|nullable',
            'detail' => 'string|max:50000|nullable'
        ]);

        $update = new Update();
        $update->version = $request->get('version');
        $update->release_date = Carbon::createFromFormat('d-m-Y', $request->get('releaseDate'))->format('Y-m-d');
        $update->name = $request->get('name');
        $update->detail = $request->get('detail');
        $update->project()->associate($project);
        $update->save();

        // Don't forget to update manually the project updated_at because it concern a project
        // And this way it will appears in first in explore last updated project
        $project->updated_at = Carbon::now();
        $project->save();

        return redirect()->route('updates_show', [Auth::user()->username, $project->slug, $update->id]);
    }

    /**
     * Destroy the update
     * @param $projectId int The project id
     * @param $updateId int the update id
     * @return \Illuminate\Http\RedirectResponse Redirect to the project show view
     */
    public function destroy($projectId, $updateId) {

        $project = $this->checkUserCanDoThis($projectId);

        $update = Update::find($updateId);
        $update->delete();

        return redirect()
            ->route('projects_show', [Auth::user()->username, $project->slug])
            ->with('success', Lang::get('pages.update.destroy_success_flash'));
    }

    /**
     * Check that the connected user is the author of the current project for which we want to edit / update / store / destroy
     * an update
     * @param $projectId int The project id
     * @return mixed 404 error if we are not the author, the project otherwise
     */
    private function checkUserCanDoThis($projectId){
        $project = Project::select('*')->with('user')->where(['id' => $projectId])->firstOrFail();
        // Check that we are the author of the project
        if(!AuthHelpers::connectedUserIs($project['user']['username'])){
            abort(404);
        }

        return $project;
    }

    /**
     * Display the "Edit update" view
     */
    public function edit($projectId, $updateId){
        $project = Project::findOrFail($projectId);

        // Check that we are the author of the update's project
        if(!AuthHelpers::connectedUserIs($project->user->username)){
            abort(404);
        }

        $update = Update::findOrFail($updateId);

        return view('updates/edit', [
            'update' => $update
        ]);
    }

    /**
     * Validate and modify an update
     */
    public function update(Request $request, $updateId){
        $update = Update::findOrFail($updateId);
        $project = Project::findOrFail($update->project_id);

        // Check that we are the author of the update's project
        if(!AuthHelpers::connectedUserIs($update->project->user->username)){
            abort(404);
        }

        // Create the form validator
        $request->validate([
            'version' => 'required|string|max:15',
            'releaseDate' => 'required|regex:'.ProjectHelper::$DATE_REGEX,
            'name' => 'string|max:50|nullable',
            'detail' => 'string|max:50000|nullable'
        ]);

        // Modify fields
        $update->name = request('name');
        $update->detail = request('detail');
        $update->version = request('version');
        // Use Carbon to generate right DB date format
        $update->release_date = \Illuminate\Support\Carbon::createFromFormat('d-m-Y', request('releaseDate'))->format('Y-m-d');

        $update->save();

        return redirect()->route('updates_show', [$project->user->username, $project->slug, $updateId])->with('success', Lang::get('pages.update.update_success_flash'));
    }
}
