<?php

namespace App\Http\Controllers;

use App\Helpers\RatingHelper;
use App\Project;
use Illuminate\Support\Facades\DB;

class ExploreController extends Controller
{

    /**
     * Show the explore view
     */
    public function show() {
        $limit = 5;
        $baseSQLSelect = 'projects.id, slug, name, detail, projects.logo, author.username, author.displayName';

        $mostRatedProjects = Project::select(DB::raw($baseSQLSelect))
                            ->addSelect(DB::raw(RatingHelper::$SQL_RATING_SELECT))
                            ->join('users as author', 'author.id', '=', 'projects.user_id')
                            ->with('followers:users.id')
                            ->orderBy('rating', 'DESC')
                            ->take($limit)
                            ->get();

        $lastUpdatedProjects = Project::select(DB::raw($baseSQLSelect))
                                ->addSelect(DB::raw(RatingHelper::$SQL_RATING_SELECT))
                                ->join('users as author', 'author.id', '=', 'projects.user_id')
                                ->with('followers:users.id')
                                ->orderBy('projects.updated_at', 'DESC')
                                ->take($limit)
                                ->get();

        $randomProjects = Project::select(DB::raw($baseSQLSelect))
                                ->addSelect(DB::raw(RatingHelper::$SQL_RATING_SELECT))
                                ->join('users as author', 'author.id', '=', 'projects.user_id')
                                ->with('followers:users.id')
                                ->inRandomOrder()
                                ->limit($limit)
                                ->get();

        return view('explore/show', [
            'mostRatedProjects' => $mostRatedProjects,
            'lastUpdatedProjects' => $lastUpdatedProjects,
            'randomProjects' => $randomProjects
        ]);
    }

}
