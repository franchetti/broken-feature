<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    /**
     * The feature's update.
     * (Function name 'update' is already taken)
     */
    public function updateModel() {
        // Since the function name is not the model's one (update),
        // we have to specify the foreign key's name.
        return $this->belongsTo(Update::class, 'update_id');
    }

    /**
     * All the feature's comments.
     */
    public function comments() {
        return $this->hasMany(Comment::class);
    }

    /**
     * All users' votes for this feature.
     */
    public function votes() {
        return $this->belongsToMany(User::class)->withPivot('upvote');
    }
}
