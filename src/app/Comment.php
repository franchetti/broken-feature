<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The comment's author.
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * The comment's feature.
     */
    public function feature() {
        return $this->belongsTo(Feature::class);
    }

    /**
     * The comment's parent (optional).
     */
    public function comment() {
        return $this->belongsTo(Comment::class);
    }

    /**
     * All users' votes on this comment.
     */
    public function votes() {
        return $this->belongsToMany(User::class)->withPivot('upvote');
    }
}
