<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * Author of the project.
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * All the project's updates.
     */
    public function updates() {
        return $this->hasMany(Update::class);
    }

    /**
     * All users that follow the project.
     */
    public function followers() {
        return $this->belongsToMany(User::class);
    }
}
