<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Auth;

class FeatureHelper
{
    /**
     * Calculate the feature's score by summing up its upvotes and downvotes.
     * @param $feature
     * @return float|int
     */
    public static function calculateVoteScore($feature) {
        return 2 * $feature->upvotes_count - $feature->voters_count;
    }

    /**
     * Checks if the currently logged user has voted on the feature.
     * @param $featureId
     * @return int|null 1 or 0 for upvote or downvote and null if user hasn't voted
     */
    public static function checkUserVote($featureId) {
        if(Auth::check()) {
            $existingFeatureVote = Auth::user()->featureVotes()->where('feature_id', $featureId)->first();
            if(!empty($existingFeatureVote)) {
                return $existingFeatureVote->pivot->upvote; // User has voted (upvote or downvote)
            }
        }
        return null; // User hasn't voted
    }
}
