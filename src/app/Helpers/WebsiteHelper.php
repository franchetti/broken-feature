<?php


namespace App\Helpers;

use Hamcrest\Text\StringStartsWith;


class WebsiteHelper
{
    public static function removePrefixes($baseStr){
        $HTTP_PREFIX = "http://";
        $HTTPS_PREFIX = "https://";

        if(substr( $baseStr, 0, strlen($HTTP_PREFIX) ) === $HTTP_PREFIX){
            return substr($baseStr, strlen($HTTP_PREFIX));
        }else if(substr( $baseStr, 0, strlen($HTTPS_PREFIX) ) === $HTTPS_PREFIX){
            return substr($baseStr, strlen($HTTPS_PREFIX));
        }
        return $baseStr;
    }
}
