<?php


namespace App\Helpers;


class RatingHelper
{
    public static $SQL_RATING_SELECT = 'ROUND(upvotes_count * 100 / voters_count) AS rating';

    /**
     * Calculate rating in percent of given 'Project' or 'Update' object.
     * @param $obj 'Project' or 'Update' object, containing upvotes_count and voters_count properties.
     * @return float|int the rating in percent, or -1 if no there's no voter.
     */
    public static function calculateRating($obj) {
        return $obj->voters_count > 0 ? round(($obj->upvotes_count * 100) / $obj->voters_count) : -1;
    }
}
