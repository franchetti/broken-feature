<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Auth;

class AuthHelpers
{

    /**
     * Check if the connected user is the same user as the username in parameter
     * (username is unique)
     * @param $username string the username we want to check
     * @return bool True if connected user and username are the same, false otherwise
     */
    public static function connectedUserIs($username) {
        return Auth::check() && Auth::user()->username == $username;
    }

    public static $USERNAME_REGEX = '/^[a-zA-Z][\w-]{1,14}$/';

}
