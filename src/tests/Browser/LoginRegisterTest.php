<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\LoginPage;
use Tests\Browser\Pages\MyProfilePage;
use Tests\Browser\Pages\RegisterPage;
use Tests\DuskTestCase;
use Throwable;

class LoginRegisterTest extends DuskTestCase
{

    use DatabaseMigrations;


    /**
     * Tries to register to the application from an empty database
     * @throws Throwable
     */
    public function testCorrectRegisterLogoutAndLogin()
    {
         $this->browse(function (Browser $browser) {
            //Go to register page
            $browser->visit('/')
                ->assertSee('Broken Feature')
                ->clickLink('Register')
                ->on(new RegisterPage);

            //Fill register form with correct informations
            $this->fillRegisterForm($browser,'test@email.com' , 'test_username', 'Tester', 'P@$$w0rd', 'P@$$w0rd');
            $browser->on(new MyProfilePage())

                //logout
                ->clickLink('Logout')
                ->on(new LoginPage)

                //login again
                ->type('@usernameInput', 'test_username')
                ->type('@passwordInput', 'P@$$w0rd')
                ->click('@submitButton')
                ->on(new MyProfilePage());

        });
    }

    /**
     * Try to login again with incorrect credentials
     * @throws Throwable
     */
    public function testRegisterLogoutAndIncorrectLogin()
    {
        $this->browse(function (Browser $browser) {
            //Go to register page
            $browser->visit('/')
                ->assertSee('Broken Feature')
                ->clickLink('Register')
                ->on(new RegisterPage);

            //Fill register form with correct informations
            $this->fillRegisterForm($browser,'test@email.com' , 'test_username', 'Tester', 'P@$$w0rd', 'P@$$w0rd');
            $browser->on(new MyProfilePage())

                //logout
                ->visit('/')
                ->clickLink('Logout')
                ->on(new LoginPage)

                //Try to login with incorrect username
                ->type('@usernameInput', 'wrong')
                ->type('@passwordInput', 'P@$$w0rd')
                ->click('@submitButton')
                ->on(new LoginPage())

                //Try to login with incorrect username
                ->type('@usernameInput', 'test_username')
                ->type('@passwordInput', 'wrong')
                ->click('@submitButton')
                ->on(new LoginPage());

        });
    }

    /**
     * Try to register with an ill-formed e-mail
     * @throws Throwable
     */
    public function testRegisterIncorrectEmail(){
        $this->browse(function (Browser $browser) {
            //Go to register page
            $browser->visit('/')
                ->assertSee('Broken Feature')
                ->clickLink('Register')
                ->on(new RegisterPage);

            //fill register form with incorrect emails
            $this->fillRegisterForm($browser, 'test', 'testusername', 'Tester', 'P@$$w0rd', 'P@$$w0rd');
            $browser->on(new RegisterPage());
            $this->fillRegisterForm($browser, 'test@hello.', 'testusername', 'Tester', 'P@$$w0rd', 'P@$$w0rd');
            $browser->on(new RegisterPage());
            $this->fillRegisterForm($browser, '', 'testusername', 'Tester', 'P@$$w0rd', 'P@$$w0rd');
            $browser->on(new RegisterPage());
        });
    }


    /**
     * Try to register with an ill-formed username
     * @throws Throwable
     */
    public function testRegisterIncorrectUsername(){
        $this->browse(function (Browser $browser) {
            //Go to register page
            $browser->visit('/')
                ->assertSee('Broken Feature')
                ->clickLink('Register')
                ->on(new RegisterPage);

            //fill register form with incorrect usernames
            $this->fillRegisterForm($browser, 'test@email.com', '1nbatstart', 'Tester', 'P@$$w0rd', 'P@$$w0rd');
            $browser->on(new RegisterPage());
            $this->fillRegisterForm($browser, 'test@hello.', 'testu$ername', 'Tester', 'P@$$w0rd', 'P@$$w0rd');
            $browser->on(new RegisterPage());
            $this->fillRegisterForm($browser, 'test@hello.', 'waytoolongtestusername', 'Tester', 'P@$$w0rd', 'P@$$w0rd');
            $browser->on(new RegisterPage());
            $this->fillRegisterForm($browser, 'test@hello.', '', 'Tester', 'P@$$w0rd', 'P@$$w0rd');
            $browser->on(new RegisterPage());
        });
    }


    /**
     * Try to register with an ill-formed password or different that the confirmation
     * @throws Throwable
     */
    public function testRegisterIncorrectPassword(){
        $this->browse(function (Browser $browser) {
            //Go to register page
            $browser->visit('/')
                ->assertSee('Broken Feature')
                ->clickLink('Register')
                ->on(new RegisterPage);

            //fill register form with incorrect usernames
            $this->fillRegisterForm($browser, 'test@email.com', 'testusername', 'Tester', 'short', 'short');
            $browser->on(new RegisterPage());
            $this->fillRegisterForm($browser, 'test@email.com', 'testusername', 'Tester', 'different', 'passwords');
            $browser->on(new RegisterPage());
            $this->fillRegisterForm($browser, 'test@email.com', 'testusername', 'Tester', '', 'P@$$w0rd');
            $browser->on(new RegisterPage());
            $this->fillRegisterForm($browser, 'test@email.com', 'testusername', 'Tester', 'P@$$w0rd', '');
            $browser->on(new RegisterPage());

        });
    }

    public static function fillRegisterForm(Browser $browser, string $email, string $username, string $displayName, string $password, string $passwordConfirm){
        $browser->on(new RegisterPage())
        ->type('@emailInput', $email)
        ->type('@usernameInput', $username)
        ->type('@displayNameInput', $displayName)
        ->type('@passwordInput', $password)
        ->type('@confirmPasswordInput', $passwordConfirm)
        ->click('@submitButton');
    }
}
