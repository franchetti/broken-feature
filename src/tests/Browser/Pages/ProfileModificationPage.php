<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class ProfileModificationPage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/profile/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
            ->assertTitle("Edit profile - BrokenFeature");
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@usernameInput' => 'input[name="username"]',
            '@displayNameInput' => 'input[name="displayName"]',
            '@emailInput' => 'input[name="email"]',
            '@websiteInput' => 'input[name="website"]',
            '@bioInput' => 'textarea[name="bio"]',
            '@submitButton' => 'button[type="submit"]',
        ];
    }

    public static function fillForm(Browser $browser, string $username, string $displayName, string $email, string $website, string $bio){
        $browser->type('@usernameInput', $username)
            ->type('@displayNameInput', $displayName)
            ->type('@emailInput', $email)
            ->type('@websiteInput', $website)
            ->type('@bioInput', $bio);
    }
}
