<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class ExplorePage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/explore';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
            ->assertTitle('Explore - BrokenFeature')
            ->assertSee('Most rated projects')
            ->assertSee('Last updated projects')
            ->assertSee('Discover random projects');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@project' => '.explore__card--project',
        ];
    }
}
