<?php

namespace Tests\Feature;

use App\Feature;
use App\Project;
use App\Update;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FeaturesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test that a user can vote on a feature.
     * The fake user will successively upvote, de-upvote, downvote and de-downvote a feature.
     *
     * @return void
     */
    public function testVote()
    {
        // Create fake data
        $user = factory(User::class)->create();
        $project = factory(Project::class)->create([
            'user_id' => $user->id,
        ]);
        $update = factory(Update::class)->create([
            'project_id' => $project->id,
        ]);
        $feature = factory(Feature::class)->create([
            'update_id' => $update->id,
        ]);

        /* User votes on his feature */

        $response = $this->actingAs($user)->post(route('features_vote', $feature->id), [
            'type' => 1,
        ]);

        // Retrieve JSON response and verify it
        $response->assertOk();
        $response->assertJson(['voteScore' => 1, 'userVote' => 1]);

        /* User retracts his upvote */

        $response2 = $this->actingAs($user)->post(route('features_vote', $feature->id), [
            'type' => 1,
        ]);

        // Retrieve JSON response and verify it
        $response2->assertOk();
        $response2->assertJson(['voteScore' => 0, 'userVote' => null]);

        /* User downvotes his feature */

        $response3 = $this->actingAs($user)->post(route('features_vote', $feature->id), [
            'type' => 0,
        ]);

        // Retrieve JSON response and verify it
        $response3->assertOk();
        $response3->assertJson(['voteScore' => -1, 'userVote' => 0]);

        /* User retracts his downvote */

        $response4 = $this->actingAs($user)->post(route('features_vote', $feature->id), [
            'type' => 0,
        ]);

        // Retrieve JSON response and verify it
        $response4->assertOk();
        $response4->assertJson(['voteScore' => 0, 'userVote' => null]);
    }
}
