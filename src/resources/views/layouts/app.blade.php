<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - BrokenFeature</title>

    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/lib/flatpickr-dark-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/lib/microtip.min.css') }}">
    <link href="{{ asset('css/fontawesome.min.css') }}" rel="stylesheet">

</head>
<body>

<form id="logout-form" action="{{ route('logout') }}" method="POST">
    @csrf
</form>

<!-- Desktop header -->
<header class="hidden sm:block header">
    <div class="container flex items-center justify-between mx-auto">

        <!-- Logo-->
        <a href="{{ route('home') }}">
            <h1 class="text-3xl">
                <span class="font-thin text-white title__double--small">Broken</span>
                <span class="font-medium uppercase text-white">Feature</span>
            </h1>
        </a>

        <!-- Search bar -->
        <form class="w-64" method="GET" action="{{route('search_show')}}">
            <input type="text" class="form-input-dark m-0 max-w-xs hidden lg:block" name="q" required="required" placeholder="{{ __('pages.header.searchProject') }}">
            <input type="submit" style="display: none" />
        </form>

        <!-- Menu -->
        <ul class="flex items-center">

            <!-- Explore page -->
            <li class="ml-12">
                <a class="menu-link {{ Request::routeIs('explore_show') ? 'menu-active' : '' }}" href="{{ route('explore_show') }}">{{ __('pages.explore.title') }}</a>
            </li>

            @guest
                <!-- Login page -->
                <li class="ml-12">
                    <a  class="menu-link {{ Request::routeIs('login') ? 'menu-active' : '' }}" href="{{ route('login') }}">{{ __('pages.login.title') }}</a>
                </li>

                <!-- Register page-->
                <li class="ml-12">
                    <a  class="menu-link {{ Request::routeIs('register') ? 'menu-active' : '' }}" href="{{ route('register') }}">{{ __('pages.register.title') }}</a>
                </li>
            @endguest

            @auth
                <!-- Logout -->
                <li class="ml-12">
                    <a class="menu-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('pages.header.logout') }}
                    </a>
                </li>

                <!-- Profile page -->
                <li class="ml-12">
                    <a href="{{ route('profile_myProfile')  }}" title="{{ __('pages.profile.title')  }}">
                        <div class="w-12">
                            @include('fragments.profile_picture', [
                                'logo' => \Illuminate\Support\Facades\Auth::user()->logo,
                                'name' => \Illuminate\Support\Facades\Auth::user()->displayName,
                                'class' => 'small'])
                        </div>
                    </a>
                </li>
            @endauth
        </ul>
    </div>
</header>

<!-- Mobile header -->
<header class="block sm:hidden header p-0 pt-2">

    <!-- hamburger menu -->
    <img id="menu__img--hamburger" class="absolute top-0 left-0 ml-3 mt-3 cursor-pointer" src="{{ asset('images/menu_hamburger.png') }}" alt="">

    <!-- Logo-->
    <h1 class="text-3xl text-center">
        <a href="{{ route('home') }}">
            <span class="font-thin text-white title__double--small">Broken</span>
            <span class="font-medium uppercase text-white">Feature</span>
        </a>
    </h1>


    <!-- Search bar -->
    <div class="mt-2 pb-6 mx-12">
        <form  method="GET" action="{{route('search_show')}}">
            <input type="text" class="form-input-dark mt-0 min-w-full" name="q" required="required" placeholder="{{ __('pages.header.searchProject') }}">
            <input type="submit" style="display: none" />
        </form>
    </div>

    <!-- menu -->
    <ul id="header__subMenu--mobile" class="hidden text-center">

        <!-- Explore page -->
        <li class="header__li--mobile border-t-0">
            <a class="menu-link {{ Request::routeIs('explore_show') ? 'menu-active' : '' }}" href="{{ route('explore_show')  }}">{{ __('pages.explore.title') }}</a>
        </li>

        @guest
            <!-- Login page -->
            <a  class="menu-link {{ Request::routeIs('login') ? 'menu-active' : '' }}" href="{{ route('login') }}">
                <li class="header__li--mobile">
                    {{ __('pages.login.title') }}
                </li>
            </a>

            <!-- Register page -->
            <a  class="menu-link {{ Request::routeIs('register') ? 'menu-active' : '' }}" href="{{ route('register') }}">
                <li class="header__li--mobile">
                    {{ __('pages.register.title') }}
                </li>
            </a>
        @endguest

        @auth
            <!-- Logout page -->
            <a class="menu-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <li class="header__li--mobile">
                    {{ __('pages.header.logout') }}
                </li>
            </a>

            <!-- My Profile page -->
            <a class="menu-link" href="{{ route('profile_myProfile')  }}" title="{{ __('pages.profile.title')  }}">
                <li class="header__li--mobile">
                    {{ __('pages.profile.title') }}
                </li>
            </a>
        @endauth
    </ul>

</header>

@yield('content')

<!-- Footer -->
<footer class="bg-indigo-darker text-white pt-20 pb-10">
    <div class="container opacity-50 text-sm font-light">

    <span>{{ __('pages.footer.madeWith') }}<i class="ml-2 mr-2 fa fa-heart text-red"></i>{{ __('pages.footer.by') }}</span>

        <ul class="mt-10 leading-6">
            <li>Loïc Dessaules</li>
            <li>Robin Demarta</li>
            <li>Thibaud Franchetti</li>
            <li>Gildas Houlmann</li>
        </ul>

        <span class="float-right -mt-5">© {{ now()->year }} Broken Feature</span>

    </div>
</footer>

<script src="{{ asset('js/lib/flatpickr.min.js') }}"></script>
<script src="{{ asset('js/utils.js') }}"></script>

<script type="text/javascript">
    /* Mobile sub menu management  */
    let mobileMenuHamburger = document.getElementById('menu__img--hamburger');
    let subMenu = document.getElementById('header__subMenu--mobile');
    subMenu.style.display = 'none' // First time, force to display:none

    mobileMenuHamburger.addEventListener('click', function() {
        if(subMenu.style.display === 'none'){
            subMenu.style.display = 'block';
        } else {
            subMenu.style.display = 'none'
        }
    });
</script>

<script type="text/javascript">
    @yield('javascript')
</script>

</body>
</html>
