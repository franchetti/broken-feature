<div class="vote @isset($class){{ $class }}@endisset">
    <form class="voteForm contents" action="{{ route($voteRoute, $itemId) }}" method="POST">
        @csrf
        <input name="type" type="hidden" value="1" />
        <button class="fas fa-chevron-up arrow arrow--up -mb-1 @if($userVote == 1) upvoted @endif"></button>
    </form>
    <span class="@if($userVote == 1) upvoted @elseif(!is_null($userVote) && $userVote == 0) downvoted @endif">{{ $itemScore }}</span>
    <form class="voteForm contents" action="{{ route($voteRoute, $itemId) }}" method="POST">
        @csrf
        <input name="type" type="hidden" value="0" />
        <button class="fas fa-chevron-down arrow arrow--down -mt-1 @if(!is_null($userVote) && $userVote == 0) downvoted @endif"></button>
    </form>
</div>
