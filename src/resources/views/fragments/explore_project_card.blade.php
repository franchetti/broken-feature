<div class="explore__card--project">
    <div class="flex">

        <div class="mr-2 md:mr-5 flex flex-col justify-center items-center">
            <div class=" w-20 flex-shrink-0">
                @include('fragments.logo', ['logo' => $project->logo, 'name' => $project->name, 'link' => route('projects_show', [$project->username, $project->slug])])
            </div>
            <div class="-mt-5 z-10">
                @include('fragments.rating', ['rating' => $project->rating])
            </div>
        </div>

        <div class="self-center">
            <a href="{{ route('projects_show', [$project->username, $project->slug]) }}">
                <h2 class="text-white font-bold inline mr-2">{{ \Illuminate\Support\Str::limit($project->name, 20, $end='...') }}</h2>
            </a>

            <br>

            <span class="subtitle">
                {{__('pages.explore.by')}}
                <a href="{{ route('profile_show', $project->username)  }}">{{ $project->displayName }}</a>
                <span class="opacity-50">{{ '@'.$project->username }}</span>
            </span>

            <br>

            <span class="subtitle">
                {{ count($project->followers) }} <i class="fas fa-users"></i> • {{ count($project->updates).' '.\Illuminate\Support\Str::plural(__('pages.explore.updates'), count($project->updates)) }}
            </span>
        </div>
    </div>


        <div>
            <p class="text-white my-2 text-sm">
                {{ \Illuminate\Support\Str::limit($project->detail, 170, $end='...') }}
            </p>

            <a class="mt-3 text-sm" href="{{ route('projects_show', [$project->username, $project->slug]) }}">{{__('pages.explore.see_project')}}</a>
        </div>

</div>
