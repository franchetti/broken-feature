@auth
    <form class="flex items-start" method="POST" action="{{ route('comments_store', $featureId) }}">
        @csrf
        @isset($parentId)
            <!-- Parent comment (optional) -->
            <input type="hidden" name="parentId" value="{{ $parentId }}"/>
        @endisset
        <!-- Text -->
        <textarea class="form-input-dark w-full mt-0 mr-2" name="text" rows="1" placeholder="{{ __('pages.feature.insert_comment') }}"></textarea>
        <!-- Submit -->
        <button type="submit" class="btn">
            {{ __('forms.buttons.post') }}
        </button>
    </form>
@endauth
@guest
    <p class="text-center subtitle">
        <a class="link-soft" href="{{ route('login') }}">{{ __('pages.feature.login_to_comment') }}</a>
    </p>
@endguest
