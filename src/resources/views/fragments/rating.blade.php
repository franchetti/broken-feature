@if($rating < 0 || is_null($rating))
    <div class="rating bg-subtitle">
        {{ __('pages.project.rating_empty') }}
    </div>
@else
    @if($rating < 30)
        <div class="rating bg-red">
    @elseif($rating < 70)
        <div class="rating bg-orange">
    @else
        <div class="rating bg-green">
    @endif
            {{ $rating }}%
        </div>
@endif
