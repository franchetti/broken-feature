@extends('layouts.app')

@section('title')
    {{ __('pages.profile_change_password.title') }}
@endsection

@section('content')
    <section class="container-bg">
        <div class="container mx-auto">

            <h1 class="h1 mb-4 text-center">{{ __('pages.profile_change_password.form_title') }}</h1>

            <div class="mt-4 container-box p-10 max-w-xl">

                <form method="POST" action="{{ route('profile_updatePassword') }}">
                    @csrf

                    <!-- Old password -->
                    <input type="password" class="form-input-dark" name="oldPassword" placeholder="{{ __('forms.placeholders.oldPassword') }}" required autofocus>
                    @error('oldPassword')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- New password -->
                    <input type="password" class="form-input-dark mt-8" name="newPassword" placeholder="{{ __('forms.placeholders.newPassword') }}" required>
                    @error('newPassword')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Confirm new password -->
                    <input type="password" class="form-input-dark" name="newPassword_confirmation" placeholder="{{ __('forms.placeholders.newPasswordConfirm') }}" required>


                    <!-- Submit -->
                    <button type="submit" class="mt-10 btn block mx-auto">
                        {{ __('forms.buttons.update') }}
                    </button>

                </form>
            </div>


        </div>
    </section>
@endsection

