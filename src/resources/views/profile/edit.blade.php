@extends('layouts.app')

@section('title')
    {{ __('pages.profile_edit.title') }}
@endsection

@section('content')
    <section class="container-bg">
        <div class="container mx-auto">

            <h1 class="h1 mb-4 text-center">{{ __('pages.profile_edit.form_title') }}</h1>

            <div class="mt-4 container-box p-10 max-w-xl">

                <form method="POST" action="{{ route('profile_update') }}" enctype="multipart/form-data">
                    @csrf

                    <!-- Username -->
                    <label class="form-label mt-4" for="username">{{ __('forms.placeholders.username') }} </label>
                    <div class="flex content-center">
                        <input type="text" class="form-input-dark mt-2" name="username" placeholder="{{ __('forms.placeholders.username') }}" value="{{old('username', $user->username)}}" maxlength="15" required autocomplete="username">
                        <span class="mt-4 input__tooltip" aria-label="{{ __('forms.tooltips.username') }}" role="tooltip" data-microtip-position="top-left">
                            <i class="far fa-question-circle subtitle"></i>
                        </span>
                    </div>
                    @error('username')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Display Name -->
                    <label class="form-label mt-4" for="displayName">{{ __('forms.placeholders.displayName') }} </label>
                    <input class="form-input-dark mt-2" type="text" name="displayName" placeholder="{{ __('forms.placeholders.displayName') }}" value="{{old('displayName', $user->displayName)}}" maxLength="25" required>
                    @error('displayName')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Email -->
                    <label class="form-label mt-4" for="email">{{ __('forms.placeholders.email') }} </label>
                    <input class="form-input-dark mt-2" type="text" name="email" placeholder="{{ __('forms.placeholders.email') }}" value="{{old('email', $user->email)}}" required>
                    @error('email')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Website -->
                    <label class="form-label mt-4" for="website">{{ __('forms.placeholders.website') }} <span class="subtitle">({{ __('forms.optional') }})</span></label>
                    <input class="form-input-dark mt-2" type="text" name="website" placeholder="{{ __('forms.placeholders.website') }}" value="{{old('website', $user->website)}}" maxlength="70">
                    @error('website')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Bio -->
                    <label class="form-label mt-4" for="bio">{{ __('forms.placeholders.bio') }} <span class="subtitle">({{ __('forms.optional') }})</span></label>
                    <textarea class="form-input-dark mt-2" name="bio" rows="3" placeholder="{{ __('forms.placeholders.bio') }}" maxlength="400">{{old('bio', $user->bio)}}</textarea>
                    @error('bio')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Profile picture -->
                    <label class="form-label mt-4" for="profilePictureFile">{{ __('forms.placeholders.profile_picture') }} <span class="subtitle">({{ __('forms.optional') }})</span></label>
                    <div class="flex">
                        @if($user->logo != null)
                            <div class="w-16 mr-6 bg-cover" style="background-image: url('{{ asset('storage/profiles/'.$user->logo) }}')"></div>
                        @endif
                        <input id="profilePictureFile" class="mt-2 form-input-dark" type="file" name="profilePictureFile" accept="image/png, image/jpeg">
                    </div>
                    @error('profilePictureFile')
                        <p class="form__error">{{ $message }}</p>
                    @enderror
                    @if($user->logo != null)
                        <div class="mt-2">
                            <input id="removeLogo" name="removeLogo" type="checkbox"><label for="removeLogo"><span class="subtitle ml-2">Remove this image </span></label>
                        </div>
                    @endif

                    <!-- Submit -->
                    <button type="submit" class="mt-10 btn block mx-auto">
                        {{ __('forms.buttons.update') }}
                    </button>

                </form>
            </div>


        </div>
    </section>
@endsection

