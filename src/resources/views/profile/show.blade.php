@extends('layouts.app')


@section('title')
    {{ __('pages.profile.title') }}
@endsection

@section('content')

    <section class="bg-indigo-darker py-4 md:py-10">
        <div class="container flex flex-col md:flex-row">

            <div class="w-24 lg:w-48 self-center md:self-auto md:flex-shrink-0">
                @include('fragments.profile_picture', ['logo' => $user->logo, 'name' => $user->displayName])
            </div>
            <div class="text-white md:px-5 flex flex-col justify-center">
                <h1 class="h1 font-bold">{{ $user->displayName }}</h1>
                <h2 class="text-md mb-2 opacity-50">{{ "@".$user->username }}</h2>

                @if(\App\Helpers\AuthHelpers::connectedUserIs($user->username))
                    <p><i class="far fa-envelope subtitle"></i> {{ $user->email }}</p>
                @endif

                @if(!empty($user->website))
                    <p>
                        <i class="fas fa-link subtitle"></i>
                        <a class="link-soft" target="_blank" href="http://{{$user->website}}"> {{ $user->website }}</a>
                    </p>
                @endif

                @if(!empty($user->bio))
                    <p class="pt-2">{{ $user->bio }}</p>
                @endif
            </div>
        </div>
        <div class="container mt-8">
            @if (\App\Helpers\AuthHelpers::connectedUserIs($user->username))
                <a href="{{ route('profile_edit') }}" class="btn btn--outline float-left" dusk="editProfileButton">
                    <i class="far fa-edit"></i> {{ __('pages.profile.update_button_text')}}
                </a>

                <a href="{{ route('profile_changePassword') }}" class="btn btn--outline float-left ml-6">
                    <i class="fas fa-key"></i> {{ __('pages.profile_change_password.title')}}
                </a>

                <form id="delete-update-form" action="{{ route('profile_destroy') }}" method="POST">
                    @csrf
                    @method('delete')
                    <button class="btn ml-6 btn btn--outline outline--red"><i class="far fa-trash-alt"></i> {{ __('pages.profile.delete_btn_text')}}</button>
                </form>

            @endif
        </div>
    </section>

    <section class="container-bg">

        <div class="mb-8 container">
            @include('fragments.flash-message')

            @if (\App\Helpers\AuthHelpers::connectedUserIs($user->username))
                <a class="btn" href="{{ route('projects_create') }}" dusk="newProjectButton"><i class="fas fa-plus"></i> {{ __('pages.project_create.title') }}</a>
            @endif
        </div>

        <div class="flex flex-col lg:flex-row container">
            <div class="w-full">
                <div id="myProjectsDiv">
                    @if (\App\Helpers\AuthHelpers::connectedUserIs($user->username))
                        <h1 class="h1">{{ __('pages.profile.my_projects')  }}</h1>
                    @else
                        <h1 class="h1">{{ __('pages.profile.their_projects')  }}</h1>
                    @endif
                    @if(count($ownProjects) > 0)
                        @foreach ($ownProjects as $project)
                            @include('fragments.large_project_card', ['project' => $project])
                        @endforeach
                    @else
                        @if (\App\Helpers\AuthHelpers::connectedUserIs($user->username))
                            <p class="subtitle">{{ __('pages.profile.no_projects_personal') }}</p>
                        @else
                            <p class="subtitle">{{ __('pages.profile.no_projects_other_person') }}</p>
                        @endif
                    @endif
                </div>

                <div class="pagination-custom">
                    {{$ownProjects->appends(['projects' => $ownProjects->currentPage(), 'follows' => $follows->currentPage()])->links()}}
                </div>

            </div>
            <div class="w-16"></div>
            <div class="w-full">
                <div id="myFollowsDiv">
                    <h1 class="h1 mt-8 lg:mt-0"> {{ __('pages.profile.my_follows')  }} </h1>
                    @if(count($follows) > 0)
                        @foreach ($follows as $follow)
                            @include('fragments.large_project_card', ['project' => $follow])
                        @endforeach
                    @else
                        @if (\App\Helpers\AuthHelpers::connectedUserIs($user->username))
                            <p class="subtitle">{{ __('pages.profile.no_follows_personal') }}</p>
                        @else
                            <p class="subtitle">{{ __('pages.profile.no_follows_other_person') }}</p>
                        @endif

                    @endif
                </div>

                <div class="pagination-custom">
                    {{$follows->appends(['follows' => $follows->currentPage(), 'projects' => $ownProjects->currentPage()])->links()}}
                </div>

            </div>
        </div>
    </section>
@endsection
