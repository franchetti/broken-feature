@extends('layouts.app')

@section('title')
{{ __('pages.login.title') }}
@endsection

@section('content')
    <main class="container-bg">
        <div class="container">

            @include('fragments.flash-message')

            <h1 class="h1 text-center">{{ __('pages.login.title') }}</h1>

            <div class="mt-4 container-box p-10 max-w-sm">

                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <!-- Username -->
                    <input class="mt-0 form-input-dark" type="text" name="username" placeholder="{{ __('forms.placeholders.username') }}" value="{{ old('username') }}" required autocomplete="username" autofocus>
                    @error('username')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Password -->
                    <input type="password" class="form-input-dark" name="password" placeholder="{{ __('forms.placeholders.password') }}" required autocomplete="current-password">
                    @error('password')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Remember me -->
                    <input type="checkbox" class="mt-6"  name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="ml-3 text-white" for="remember"> {{ __('forms.labels.rememberMe') }}</label>
                    <br>

                    <!-- Submit -->
                    <button type="submit" class="mt-6 btn mx-auto block">
                        {{ __('forms.buttons.login') }}
                    </button>

                    <!-- if we want forgot password functionnality
                    @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                    @endif-->

                </form>
            </div>

        </div>
    </main>
@endsection
