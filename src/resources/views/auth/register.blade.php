@extends('layouts.app')

@section('title')
{{ __('pages.register.title') }}
@endsection

@section('content')
    <main class="container-bg">
        <div class="container">

            <h1 class="h1 text-center">{{ __('pages.register.title') }}</h1>

            <div class="mt-4 container-box p-10 max-w-sm">

                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <!-- E-mail -->
                    <input class="mt-0 form-input-dark" type="email" name="email" placeholder="{{ __('forms.placeholders.email') }}" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Username -->
                    <div class="flex">
                        <input type="text" class="form-input-dark" name="username" placeholder="{{ __('forms.placeholders.username') }}" value="{{ old('username') }}" maxlength="15" required autocomplete="username">
                        <span class="input__tooltip" aria-label="{{ __('forms.tooltips.username') }}" role="tooltip" data-microtip-position="top-left">
                            <i class="far fa-question-circle subtitle"></i>
                        </span>
                    </div>
                    @error('username')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- displayName -->
                    <input type="text" class="form-input-dark" name="displayName" placeholder="{{ __('forms.placeholders.displayName') }}" value="{{ old('displayName') }}" maxlength="25" required autocomplete="displayName">
                    @error('displayName')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Password -->
                    <input type="password" class="form-input-dark" name="password" placeholder="{{ __('forms.placeholders.password') }}" required autocomplete="new-password">
                    @error('password')
                        <p class="form__error">{{ $message }}</p>
                    @enderror

                    <!-- Confirm password -->
                    <input type="password" class="form-input-dark" name="password_confirmation" placeholder="{{ __('forms.placeholders.passwordConfirm') }}" required autocomplete="new-password">

                    <!-- Submit -->
                    <button type="submit" class="mt-6 btn mx-auto block">
                        {{ __('forms.buttons.register') }}
                    </button>

                </form>
            </div>

        </div>
    </main>
@endsection
