@extends('layouts.app')

@section('title')
    {{ __('pages.explore.title') }}
@endsection

@section('content')
    <section class="container-bg">
        <div class="container mx-auto">

            <h1 class="h1 mb-4">{{ __('pages.explore.most_rated_projects') }}</h1>

            <div class="container-box p-4 md:p-6">
                <div class="explore__cards">
                    @foreach ($mostRatedProjects as $project)
                        @include('fragments.explore_project_card', ['project' => $project])
                    @endforeach
                </div>
            </div>

            <h1 class="h1 mb-4 mt-10">{{ __('pages.explore.last_updated_projects') }}</h1>

            <div class="container-box p-4 md:p-6">
                <div class="explore__cards">
                    @foreach ($lastUpdatedProjects as $project)
                        @include('fragments.explore_project_card', ['project' => $project])
                    @endforeach
                </div>
            </div>

            <h1 class="h1 mb-4 mt-10">{{ __('pages.explore.discover_random_projects') }}</h1>

            <div class="container-box p-4 md:p-6">
                <div class="explore__cards">
                    @foreach ($randomProjects as $project)
                        @include('fragments.explore_project_card', ['project' => $project])
                    @endforeach
                </div>
            </div>

        </div>
    </section>
@endsection
