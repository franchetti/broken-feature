@extends('layouts.app')

@section('title')
    {{ $project->name }}
@endsection

@section('content')
    <!-- Banner -->
    <section class="py-16 md:py-32 lg:py-48 bg-indigo-dark bg-center bg-cover h-0 flex justify-center items-center" @if(isset($project->banner))
        style="background-image: url('{{ asset('storage/projects/banners/'.$project->banner) }}')">
        @else
            ><span class="font-mono text-center text-3xl text-indigo-lighter">//<b>TODO</b> find a banner</span>
        @endif</section>

    <!-- Presentation section -->
    <section class="bg-indigo-darker">
        <div class="container md:flex">
            <!-- Logo -->
            <div class="md:w-2/12">
                <div class="-translate-y-1/2 w-24 md:w-auto md:px-4 -mb-24 md:-mb-0 mx-auto items-center flex flex-col">
                    <div class="w-full">
                        @include('fragments.logo', ['logo' => $project->logo, 'name' => $project->name, 'class' => 'dark-border'])
                    </div>
                    <div class="-mt-4 z-10">
                        @include('fragments.rating', ['rating' => $project->rating])
                    </div>
                </div>
            </div>

            <!-- Title and description -->
            <div class="container-content pt-12 md:py-8">
                <h1 class="h1">{{ $project->name }}</h1>
                <h3 class="h3">
                    {{ __('pages.project.by_user') }}
                    <a href="{{ route('profile_show', $project->user->username)  }}">{{ $project->user->displayName }}</a>
                    <span class="opacity-50">{{ '@'.$project->user->username }}</span>
                </h3>

                <p class="mt-3"><a target="_blank" href="http://{{$project->website}}">{{ $project->website }}</a></p>
                <p class="mt-3">{{ $project->detail }}</p>
                <p class="subtitle mt-3">{{ __('pages.project.released') . ' ' . $project->release_date }}</p>
            </div>


            <!-- Follow / Unfollow button -->
            <form id="followForm" class="md:w-2/12 py-8 flex items-center justify-center flex-col" method="POST" action="{{ route('projects_follow', ['id' => $project->id]) }}">
                @csrf
                <button class="btn @if($isFollowing) btn--outline @endif">@if($isFollowing) {{ __('pages.project.unfollow_btn') }} @else {{ __('pages.project.follow_btn') }} @endif</button>
                <span>
                    <span class="followersCount subtitle mt-1">{{ count($project->followers) }}</span>
                    <i class="subtitle mt-1 fas fa-users"></i>
                </span>
            </form>


        </div>
    </section>

    <!-- Content section -->
    <section class="container-bg">
        <div class="container">
            <div class="container-content">

                @include('fragments.flash-message')

                @if (\App\Helpers\AuthHelpers::connectedUserIs($project->user->username))
                    <div class="flex flex-wrap items-start justify-between">
                        <a class="btn mb-4" href="{{ route('updates_create', [$project->id]) }}">
                            <i class="fas fa-plus"></i> {{ __('pages.update_create.title') }}
                        </a>

                        <div class="flex">
                            <a class="btn btn--outline flex-none" href="{{ route('projects_edit', [$project->id]) }}">
                                <i class="far fa-edit"></i>
                                <span class="hidden md:inline"> {{ __('pages.project.edit_btn') }}</span>
                            </a>
                            <form id="delete-update-form" action="{{ route('projects_destroy', $project->id) }}" method="POST">
                                @csrf
                                @method('delete')
                                <button class="btn btn--outline outline--red ml-6 float-right">
                                    <i class="far fa-trash-alt"></i>
                                    <span class="hidden md:inline"> {{ __('pages.project.delete_btn') }}</span>
                                </button>
                            </form>
                        </div>
                    </div>
                @endif
                <h1 class="h1">{{ __('pages.update.title') }}</h1>
                @if(count($project->updates) == 0)
                    <p class="subtitle">{{ __('pages.project.no_updates') }}.</p>
                @else
                    @foreach($project->updates as $u)
                        <div class="container-box mt-8 cursor-pointer" onclick="window.location='{{ route('updates_show', [
                        'username' => $project->user->username,
                        'projectSlug' => $project->slug,
                        'updateId' => $u->id]) }}'">
                            <div class="item-content">
                                <div>
                                    <h3 class="h3">
                                        {{ $u->version }}
                                        @if(!empty($u->name))
                                            {{ ' • ' . $u->name}}
                                        @endif
                                    </h3>
                                    <p class="subtitle">
                                        {{ __('pages.project.released').' '.$u->release_date.' • '.count($u->features).' '.Str::plural(__('pages.project.feature_count'), count($u->features)) }}
                                    </p>
                                </div>
                                @include('fragments.rating', ['rating' => $u->rating])
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@endsection


@section('javascript')
    const followForm = document.getElementById('followForm');
    const CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    const followBtnElem = followForm.querySelector('.btn');
    const followersCountElem = followForm.querySelector('.followersCount')

    followForm.onsubmit = (event) => {
        event.preventDefault();

        fetch(followForm.action, {
            method: followForm.method,
            headers: {
                'X-CSRF-TOKEN': CSRFToken
            }
        })
        .then(response => response.json())
        .then(data => {

            // Update text elem
            followersCountElem.textContent = data.followersCount;
            followBtnElem.textContent = data.newBtnText;

            // Btn classes managemen
            // Was unfollow -> now is follow
            if(followBtnElem.classList.contains('btn--outline')){
                followBtnElem.classList.remove('btn--outline')
            }
            // Was follow => now is unfollow
            else{
                followBtnElem.classList.add('btn--outline')
            }

            // Remove the focus on the button
            followBtnElem.blur()
        })
        .catch(() => window.location.href = "/login");
    }
@endsection
