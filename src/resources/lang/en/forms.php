<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms management
    |--------------------------------------------------------------------------
    |
    | All words related to forms constants categorized by labels,
    | placeholders, buttons, ...
    |
    */

    'optional' => 'optional',

    // Labels
    'labels' => [
        'rememberMe' => 'Remember Me',
    ],

    // Placeholders
    'placeholders' => [
        'username' => 'Username',
        'displayName' => 'Display name',
        'password' => 'Password',
        'passwordConfirm' => 'Confirm your password',
        'oldPassword' => 'Old password',
        'newPassword' => 'New password',
        'newPasswordConfirm' => 'New password confirmation',
        'email' => 'E-mail',
        'name' => 'Name',
        'detail' => 'Detail',
        'release_date' => 'Release date',
        'logo' => 'Logo',
        'banner' => 'Banner',
        'version' => 'Version code',
        'profile_picture' => 'Profile Picture',
        'bio' => 'Bio',
        'website' => 'Website',
    ],

    // Buttons
    'buttons' => [
        'login' => 'Login',
        'register' => 'Register',
        'create' => 'Create',
        'update' => 'Update',
        'post' => 'Post',
    ],

    // Custom form errors
    'custom_errors' => [
        'project_slug' => 'Generated URL for this name already exists, choose another one'
    ],

    // Tooltips inside forms
    'tooltips' => [
        'username' => 'Begin with [A-Za-z] followed by [a-z0-9-_] maximum 14',
        'release_date' => 'Must respect the [dd-mm-YYYY] format',
        'project_name' => 'Must contains at least one letter or number',
        'version' => 'A version code for your update like v.1.0.1'
    ]

];
