<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdatesTable extends Migration
{
    /**
     * Create the updates table.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('updates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50)->nullable();
            $table->text('detail')->nullable();
            $table->string('version', 15);
            $table->date('release_date');
            $table->unsignedInteger('voters_count')->default(0); // All feature votes in this update
            $table->unsignedInteger('upvotes_count')->default(0); // Number of feature upvotes in this update
            $table->unsignedBigInteger('project_id');
            $table->timestamps();

            $table->foreign('project_id')
                ->references('id')->on('projects')
                ->onDelete('cascade');
        });
    }

    /**
     * Drops the updates table.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('updates');
    }
}
