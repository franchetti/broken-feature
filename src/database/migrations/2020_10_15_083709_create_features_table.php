<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeaturesTable extends Migration
{
    /**
     * Creates the features table.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50);
            $table->text('detail');
            $table->unsignedInteger('voters_count')->default(0); // All feature votes
            $table->unsignedInteger('upvotes_count')->default(0); // Number of upvotes
            $table->unsignedBigInteger('update_id');
            $table->timestamps();

            $table->foreign('update_id')
                ->references('id')->on('updates')
                ->onDelete('cascade');
        });
    }

    /**
     * Drops the features table.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('features');
    }
}
