<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'text' => 'Wow that\'s an awesome addition. Was not expecting that!',
            'user_id' => 1,
            'feature_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comments')->insert([
            'text' => 'Thanks for the comment :)',
            'user_id' => 5,
            'feature_id' => 1,
            'comment_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comments')->insert([
            'text' => 'Your are welcome.',
            'user_id' => 1,
            'feature_id' => 1,
            'comment_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comments')->insert([
            'text' => 'Yeah they never talked about it before. Great surprise nonetheless.',
            'user_id' => 4,
            'feature_id' => 1,
            'comment_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comments')->insert([
            'text' => 'This is a revolution.',
            'user_id' => 2,
            'feature_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comments')->insert([
            'text' => 'Nah, not too hyped about this.',
            'user_id' => 3,
            'feature_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
